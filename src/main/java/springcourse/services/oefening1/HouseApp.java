package springcourse.services.oefening1;

import springcourse.services.oefening1.services.*;

public class HouseApp {

    public static void main(String[] args) {

        CleaningTool broom = new Broom();
        CleaningTool vacuumCleaner = new VaccuumCleaner();
        CleaningTool sponge = new Sponge();

        CleaningServiceImpl omar = new CleaningServiceImpl();
        omar.setCleaningTool(broom);

        CleaningServiceImpl nika = new CleaningServiceImpl();
        nika.setCleaningTool(vacuumCleaner);

        CleaningServiceImpl anja = new CleaningServiceImpl();
        anja.setCleaningTool(sponge);

        omar.clean();
        nika.clean();
        anja.clean();


    }
}
