package springcourse.services.oefening2;


import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcourse.services.oefening2.services.CleaningService;

public class HouseApp {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        CleaningService omar = configurableApplicationContext.getBean("omar",CleaningService.class);
        CleaningService nika = configurableApplicationContext.getBean("nika",CleaningService.class);
        CleaningService anja = configurableApplicationContext.getBean("anja",CleaningService.class);

        omar.clean();
        nika.clean();
        anja.clean();



    }
}
