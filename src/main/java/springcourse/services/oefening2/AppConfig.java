package springcourse.services.oefening2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springcourse.services.oefening2.services.*;

@Configuration
public class AppConfig {

    @Bean
    CleaningTool broom() {
        return new Broom();
    }

    @Bean
    CleaningTool vacuumCleaner(){
        return new VaccuumCleaner();
    }

    @Bean
    CleaningTool sponge(){
        return new Sponge();
    }

    @Bean
    CleaningServiceImpl omar(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(broom());
        return cleaningService;
    }

    @Bean
    CleaningServiceImpl nika(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(vacuumCleaner());
        return cleaningService;
    }

    @Bean
    CleaningServiceImpl anja(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(sponge());
        return cleaningService;
    }

}
