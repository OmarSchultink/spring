package springcourse.services.oefening5klas.domain;


import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springcourse.services.oefening5klas.services.CleaningTool;

@Component
@Order(4)
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
public class DisposableDuster implements CleaningTool {
    private boolean used = false;

    public void doClean() {
        if (used) {
            System.out.println("I am already used, take another duster");
        }else {
            System.out.println("Wipe the dust away");
            used =true;
        }
    }
}
