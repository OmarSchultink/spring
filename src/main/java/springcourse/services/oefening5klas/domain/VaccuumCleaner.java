package springcourse.services.oefening5klas.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springcourse.services.oefening5klas.services.CleaningTool;

@Component
@Order(2)
@Scope(value = "prototype")
public class VaccuumCleaner implements CleaningTool {

    public void doClean() {
        System.out.println("Zuuuuuuuuuu");
    }
}
