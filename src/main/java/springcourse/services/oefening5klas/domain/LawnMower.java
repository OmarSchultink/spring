package springcourse.services.oefening5klas.domain;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import springcourse.services.oefening5klas.services.GardeningTool;
@Component
@Primary
@Scope(value = "prototype")
public class LawnMower implements GardeningTool {

    public void doGardenJob() {
        System.out.println("Mowing the lawn");
    }
}
