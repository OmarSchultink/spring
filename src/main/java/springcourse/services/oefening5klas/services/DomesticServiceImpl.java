package springcourse.services.oefening5klas.services;

public class DomesticServiceImpl implements DomesticService {
    private CleaningService cleaningService;
    private GardeningService gardeningService;

    public void setCleaningService(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    public void setGardeningService(GardeningService gardeningService) {
        this.gardeningService = gardeningService;
    }

    public void runHousehold() {
        gardeningService.garden();
        cleaningService.clean();

    }
}
