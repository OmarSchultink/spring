package springcourse.services.oefening5klas.services;

public interface GardeningTool {
    public void doGardenJob();
}
