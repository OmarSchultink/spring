package springcourse.services.oefening4part2;

import org.springframework.context.annotation.*;
import springcourse.services.oefening4part2.services.*;

@Configuration
public class AppConfig {


    @Bean
    @Primary
    @Scope(value = "prototype")
    public CleaningTool broom() {
        return new Broom();
    }

    @Bean
    @Scope(value = "prototype")
    public CleaningTool vacuumCleaner() {
        return new VaccuumCleaner();
    }

    @Bean
    @Scope(value = "prototype")
    public CleaningTool sponge() {
        return new Sponge();
    }

    @Bean
    @Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
    public CleaningTool disposableDuster() {
        return new DisposableDuster();
    }

    @Bean
    @Primary
    public GardeningTool lawnMower() {
        return new LawnMower();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Lazy
    public GardeningService tuinman(GardeningTool gardeningTool) {
        GardeningServiceImpl gardeningService = new GardeningServiceImpl();
        gardeningService.setGardeningTool(gardeningTool);
        return gardeningService;
    }

    @Bean
    @Lazy
    public CleaningService poetsvrouw(CleaningTool cleaningTool) {
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(cleaningTool);
        return cleaningService;
    }

    @Bean
    @Lazy
    public DomesticService domesticService() {
        DomesticServiceImpl ds = new DomesticServiceImpl();
        ds.setCleaningService(poetsvrouw(broom()));
        ds.setGardeningService(tuinman(lawnMower()));
        return ds;
    }


}
