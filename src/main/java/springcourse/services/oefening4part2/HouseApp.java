package springcourse.services.oefening4part2;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcourse.services.oefening4part2.services.DomesticService;

public class HouseApp {
    public static void main(String[] args) {
        try(ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)){
            System.out.println("Container intialized");

            DomesticService domesticService = ctx.getBean("domesticService",DomesticService.class);
            domesticService.runHousehold();
        }
    }
}
