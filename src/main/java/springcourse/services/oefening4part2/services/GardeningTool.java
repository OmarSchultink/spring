package springcourse.services.oefening4part2.services;

public interface GardeningTool {
    public void doGardenJob();
}
