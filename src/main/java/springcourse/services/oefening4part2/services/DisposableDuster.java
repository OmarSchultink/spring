package springcourse.services.oefening4part2.services;


public class DisposableDuster implements CleaningTool {
    private boolean used = false;

    public void doClean() {
        if (used) {
            System.out.println("I am already used, take another duster");
        }else {
            System.out.println("Wipe the dust away");
            used =true;
        }
    }
}
