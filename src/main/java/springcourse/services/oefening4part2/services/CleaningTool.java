package springcourse.services.oefening4part2.services;

public interface CleaningTool {
    public void doClean();
}
