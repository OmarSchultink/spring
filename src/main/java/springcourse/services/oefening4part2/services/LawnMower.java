package springcourse.services.oefening4part2.services;

public class LawnMower implements GardeningTool {

    public void doGardenJob() {
        System.out.println("Mowing the lawn");
    }
}
