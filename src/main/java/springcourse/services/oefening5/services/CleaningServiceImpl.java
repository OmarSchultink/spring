package springcourse.services.oefening5.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CleaningServiceImpl implements CleaningService {
    private CleaningTool cleaningTool;

    @Autowired
    public CleaningServiceImpl(CleaningTool cleaningTool){
        this.cleaningTool = cleaningTool;
    }

   /* public void setCleaningTool(CleaningTool cleaningTool) {
        this.cleaningTool = cleaningTool;
    }*/

    public void clean() {
        System.out.println("Cleaning the house");
        cleaningTool.doClean();

    }

    public CleaningServiceImpl() {
    }
}
