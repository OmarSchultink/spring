package springcourse.services.oefening5.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class GardeningServiceImpl implements GardeningService {
    private GardeningTool gardeningTool;

    @Autowired
    public GardeningServiceImpl(GardeningTool gardeningTool){
        this.gardeningTool = gardeningTool;
    }

    public GardeningServiceImpl() {
    }

    /* public void setGardeningTool(GardeningTool gardeningTool) {
        this.gardeningTool = gardeningTool;
    }*/

    public void garden() {
        System.out.println("Working in the garden");
        gardeningTool.doGardenJob();
    }


    public void init(){
        System.out.println("GardeningService prepared for work");
    }

    public void destroy(){
        System.out.println("GardeningService cleaning up");
    }
}
