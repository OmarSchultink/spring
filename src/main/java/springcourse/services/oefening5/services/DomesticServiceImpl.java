package springcourse.services.oefening5.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import springcourse.services.oefening4part2.services.CleaningServiceImpl;
import springcourse.services.oefening4part2.services.GardeningServiceImpl;
import springcourse.services.oefening4part2.services.GardeningTool;

@Component("domesticService")
public class DomesticServiceImpl implements DomesticService {
    private CleaningService cleaningService;
    private GardeningService gardeningService;

    @Autowired
    public DomesticServiceImpl(CleaningService cleaningService,GardeningService gardeningService){
        this.cleaningService = cleaningService;
        this.gardeningService = gardeningService;
    }


    public DomesticServiceImpl(){

    }



    public void setCleaningService(CleaningService cleaningService) {
        this.cleaningService = cleaningService;
    }

    public void setGardeningService(GardeningService gardeningService) {
        this.gardeningService = gardeningService;
    }



    public void runHousehold() {
        gardeningService.garden();
        cleaningService.clean();

    }



}
