package springcourse.services.oefening5.services;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Sponge implements CleaningTool {

    public void doClean() {
        System.out.println("Pffffwwipfwwwi");
    }
}
