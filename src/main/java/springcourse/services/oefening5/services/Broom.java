package springcourse.services.oefening5.services;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class Broom implements CleaningTool {

    public Broom() {
    }

    public void doClean() {
        System.out.println("Scrub, scrub");
    }
}


