package springcourse.services.oefening5.services;

public interface GardeningTool {
    public void doGardenJob();
}
