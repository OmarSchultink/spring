package springcourse.services.oefening5.services;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class LawnMower implements GardeningTool {

    public void doGardenJob() {
        System.out.println("Mowing the lawn");
    }
}
