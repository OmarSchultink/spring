package springcourse.services.oefening5;

import org.springframework.context.annotation.*;
import org.springframework.core.annotation.Order;
import springcourse.services.oefening4part2.services.*;


@Configuration
@ComponentScan(lazyInit = true)
public class AppConfig {



    @Bean
    @Primary
    public GardeningService lawnMowerService() {
        GardeningServiceImpl gardeningService = new GardeningServiceImpl();
        gardeningService.setGardeningTool(new LawnMower());
        return gardeningService;
    }

    @Bean
    @Order(1)
    public CleaningService broomService() {
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(new Broom());
        return cleaningService;

    }

    @Bean
    @Order(3)
    public CleaningService spongeService() {
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(new Sponge());
        return cleaningService;

    }

    @Bean
    @Order(2)
    public CleaningService vacuumCleanerService() {
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(new VaccuumCleaner());
        return cleaningService;

    }
}
