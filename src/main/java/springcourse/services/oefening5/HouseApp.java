package springcourse.services.oefening5;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcourse.services.oefening5.services.*;

public class HouseApp {
    public static void main(String[] args) {
        try (ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class)) {
            System.out.println("Container intialized");


            DomesticService domestic = ctx.getBean("domesticService",DomesticService.class);
            domestic.runHousehold();


        }
    }
}
