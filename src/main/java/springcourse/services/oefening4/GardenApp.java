package springcourse.services.oefening4;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcourse.services.oefening4.services.GardeningService;


public class GardenApp {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        GardeningService nanette = configurableApplicationContext.getBean("nanette",GardeningService.class);

        nanette.garden();

    }
}
