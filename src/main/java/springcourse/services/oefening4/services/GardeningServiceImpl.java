package springcourse.services.oefening4.services;

public class GardeningServiceImpl implements GardeningService {
    private GardeningTool gardeningTool;

    public void setGardeningTool(GardeningTool gardeningTool) {
        this.gardeningTool = gardeningTool;
    }

    public void garden() {
        System.out.println("Working in the garden");
        gardeningTool.doGardenJob();
    }

    public void init(){
        System.out.println("GardeningService prepared for work");
    }

    public void destroy(){
        System.out.println("GardeningService cleaning up");
    }
}
