package springcourse.services.oefening4.services;

public interface GardeningTool {
    public void doGardenJob();
}
