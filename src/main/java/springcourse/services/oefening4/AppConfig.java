package springcourse.services.oefening4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springcourse.services.oefening4.services.GardeningService;
import springcourse.services.oefening4.services.GardeningServiceImpl;
import springcourse.services.oefening4.services.GardeningTool;
import springcourse.services.oefening4.services.LawnMower;

@Configuration
public class AppConfig {

    @Bean
    public GardeningTool lawnMower() {
        return new LawnMower();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public GardeningService nanette(GardeningTool gardeningTool) {
        GardeningServiceImpl gardeningService = new GardeningServiceImpl();
        gardeningService.setGardeningTool(gardeningTool);
        return gardeningService;
    }
}
