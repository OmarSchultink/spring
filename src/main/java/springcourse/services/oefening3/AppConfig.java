package springcourse.services.oefening3;

import org.springframework.context.annotation.*;
import springcourse.services.oefening3.services.*;

@Configuration
public class AppConfig {

    @Bean
    @Scope(value = "prototype")
    CleaningTool broom() {
        System.out.println("Creating new broom interface");
        return new Broom();
    }

    @Bean
    @Scope(value = "prototype")
    CleaningTool vacuumCleaner(){
        System.out.println("Creating new vacuum cleaner interface");
        return new VaccuumCleaner();
    }

    @Bean
    @Scope(value = "prototype")
    CleaningTool sponge(){
        System.out.println("Creating new sponge interface");
        return new Sponge();
    }

    @Bean
    @Scope(value = "prototype",proxyMode = ScopedProxyMode.INTERFACES)
    CleaningTool disposableDuster(){
        System.out.println("Creating new disposable duster interface");
        return new DisposableDuster();
    }

    @Bean
    @Lazy
    CleaningServiceImpl lieuwe(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(disposableDuster());
        System.out.println("Setting disposable duster as tool for lieuwe");
        return cleaningService;
    }


    @Bean(name = "omarCleaning")
    @Lazy
    CleaningServiceImpl omar(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(broom());
        System.out.println("Setting broom as tool for omar");
        return cleaningService;
    }

    @Bean(name = "nikaCleaning")
    @Lazy
    CleaningServiceImpl nika(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(vacuumCleaner());
        System.out.println("Setting vacuum cleaner as tool for nika");
        return cleaningService;
    }

    @Bean(name = "anjaCleaning")
    @Lazy
    CleaningServiceImpl anja(){
        CleaningServiceImpl cleaningService = new CleaningServiceImpl();
        cleaningService.setCleaningTool(sponge());
        System.out.println("Setting sponge as tool for anja");
        return cleaningService;
    }

}