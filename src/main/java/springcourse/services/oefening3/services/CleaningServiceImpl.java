package springcourse.services.oefening3.services;

public class CleaningServiceImpl implements CleaningService {
    private CleaningTool cleaningTool;

    public void setCleaningTool(CleaningTool cleaningTool) {
        this.cleaningTool = cleaningTool;
    }


    public void clean() {
        System.out.println("Cleaning the house");
        cleaningTool.doClean();
    }
}
