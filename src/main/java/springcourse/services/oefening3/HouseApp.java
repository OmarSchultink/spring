package springcourse.services.oefening3;


import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springcourse.services.oefening3.services.CleaningService;

public class HouseApp {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        CleaningService omar = configurableApplicationContext.getBean("omarCleaning", CleaningService.class);
        CleaningService nika = configurableApplicationContext.getBean("nikaCleaning", CleaningService.class);
        CleaningService anja = configurableApplicationContext.getBean("anjaCleaning", CleaningService.class);
        CleaningService lieuwe = configurableApplicationContext.getBean("lieuwe",CleaningService.class);

        omar.clean();
        nika.clean();
        anja.clean();
        lieuwe.clean();
        lieuwe.clean();
        lieuwe.clean();



    }
}
